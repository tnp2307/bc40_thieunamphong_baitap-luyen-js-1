let bai1 = () => {
  let content = "";
  let thisRound = "";
  for (var i = 0; i < 100; i += 10) {
    for (var x = 1; x <= 10; x++) {
      let column = i + x;
      thisRound += `<span class ="mr-2">${column}</span>`;
    }
    content += `<div class="mb-2">${thisRound}</div></br>`;
    thisRound = "";
  }
  document.getElementById("ketQua1").innerHTML = content;
};

let numberArr = [];

function timSoNguyenTo(x) {
  for (var i = 2; i <= Math.sqrt(x); i++) {
    if (x % i == 0) {
      return "";
    }
  }
  return x;
}
let bai2 = () => {
  let daySoNguyenTo = "";
  let soNguyenTo = 0;
  let number = document.getElementById("txt-number-bai2").value * 1;
  numberArr.push(number);
  for (var index = 0; index < numberArr.length; index++) {
    soNguyenTo = timSoNguyenTo(numberArr[index]);
    if (soNguyenTo != "") {
      daySoNguyenTo += `${soNguyenTo} `;
    }
  }

  document.getElementById(
    "bai2-arr"
  ).innerHTML = `Dãy số hiện tại : ${numberArr}`;
  document.getElementById(
    "ketQua2"
  ).innerHTML = `Các số nguyên tố trong mảng là : ${daySoNguyenTo}`;
};

let bai3 = () => {
  let number = document.getElementById("txt-number-bai3").value * 1;
  let S = 0;
  let tong = 0;
  for (var i = 2; i <= number; i++) {
    tong = tong + i;
  }
  S = tong + 2 * number;
  document.getElementById("ketQua3").innerHTML = `Tổng S : ${S}`;
};
let bai4 = () => {
  let number = document.getElementById("txt-number-bai4").value * 1;
  let uocArr = [];
  for (var i = 1; i <= number; i++) {
    if (number % i == 0) {
      uocArr.push(i);
    }
  }

  document.getElementById(
    "ketQua4"
  ).innerHTML = `Các ước số của n là: ${uocArr}`;
};

let bai5 = () => {
  let number = document.getElementById("txt-number-bai5").value;
  let stringArr = [...number];
  stringArr.reverse();
  console.log(stringArr);
  let numberReverse = "";
  stringArr.forEach((char) => {
    numberReverse += `${char}`;
  });
  document.getElementById(
    "ketQua5"
  ).innerHTML = `Số đảo ngược là: ${numberReverse}`;
};

let bai6 = () => {
  let sum = 0;
  let number = 0;
  for (var i = 1; sum <= 100; i++) {
    sum += i;
    number = i - 1;
  }
  document.getElementById("ketQua6").innerHTML = `Số x là: ${number}`;
};

let bai7 = () => {
  let content = "";
  let number = document.getElementById("txt-number-bai7").value;
  for (var i = 0; i <= 10; i++) {
    let tr = ` <div> ${number} x ${i}=${number * i}</div>`;
    content += tr;
  }
  document.getElementById("ketQua7").innerHTML = `Bảng cửu chương: ${content}`;
};
let bai8 = () => {
  let players = [[], [], [], []];
  let cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  var i = 0;
  let turnCount = 0;
  let content = "";
  let playerCount = 1;
  for (let x = 0; x < players.length; x++) {
    for (i = turnCount; i < cards.length; i += 4) {
      players[x].push(cards[i]);
    }
    turnCount++;
  }
  players.forEach((element) => {
    let tr = `<div>Player ${playerCount} : ${element}</div>`;
    playerCount++;
    content += tr;
  });
  document.getElementById(
    "ketQua8"
  ).innerHTML = `Show bài của các player ${content}`;
};
let bai9 = () => {
  let m = document.getElementById("txt-number-m").value;
  let n = document.getElementById("txt-number-n").value;
  let soCho = 0;
  let soGa = 0;
  soGa = (4 * m - n) / 2;
  soCho = m - soGa;
  document.getElementById(
    "ketQua9"
  ).innerHTML = `Số chó là ${soCho}, số gà là ${soGa}`;
};

let bai10 = () => {
  let hour = document.getElementById("txt-number-gio").value;
  let minute = document.getElementById("txt-number-phut").value;
  let angle =0
  let hourAngle = (hour/12)*360+ (minute/60)*(360/12)
  let minuteAngle = (minute/5/12)*360
  if (hourAngle>minuteAngle){
    angle = hourAngle- minuteAngle
  } else{
    angle = minuteAngle- hourAngle
  }
  document.getElementById(
    "ketQua10"
  ).innerHTML = `Góc lệch giữa kim giờ và phút là ${angle}`;
};
